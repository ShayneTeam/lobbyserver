﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CSharp.RuntimeBinder;
using Nettention.Proud;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GunnersBattleCommon;
using Google.Apis.AndroidPublisher.v2;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using CouchbaseShared;

namespace LobbyServer
{
    partial class LobbyServer
    {
        NetServer _netServer = new NetServer();

        ThreadPool _networkerThreadPool = new ThreadPool(8);
        ThreadPool _userworkerThreadPool = new ThreadPool(8);

        LobbyS2C.Proxy _S2CProxy = new LobbyS2C.Proxy();
        LobbyC2S.Stub _C2SStub = new LobbyC2S.Stub();

        AndroidPublisherService _androidPublisherService;

        System.Threading.Timer _refreshLobbyStateTimerReference;

        string _currentServerGuid = System.Guid.NewGuid().ToString();
        string _currentServerPublicIP = new System.Net.WebClient().DownloadString("https://api.ipify.org/");

        public string serverName = "";

        // public ---------------------------------------------------------------------------------------------------------------------
        public LobbyServer()
        {
            InitNetServer();

            InitRMI();

            // 구글 영수증 검증을 위한 구글 서비스 초기화
            InitGoogleAndroidPublisherService();
        }

        public void Start()
        {
            StartServerParameter sp = new StartServerParameter();
            sp.protocolVersion = new Nettention.Proud.Guid(GunnersBattleCommon.Vars.lobbyServerProtocolVersion);
            sp.tcpPorts = new IntArray();
#if DEBUG
            sp.tcpPorts.Add(20000);
#else
            sp.tcpPorts.Add(50000);
#endif

            // 게임 서버의 로드밸런싱(load balancing)을 위해 L4 스위치 뒤에 서버 호스트들을 설치하는 경우, 규칙을 L4 스위치에 적용을 해야함
            // ProudNet은 각 클라이언트에 대해 1개의 TCP 연결과 1개의 가상 UDP 연결을 유지하기 때문 
            // 따라서 같은 IP로부터 들어오는 클라이언트 연결은 언제나 같은 L4 스위치 뒤의 서버로 매핑되어야 함 
            // 만약 이러한 설정이 없으면 ProudNet의 UDP 통신과 직접 P2P 통신 기능이 정상적으로 작동하지 못함
            // 공유기 또는 L4 스위치 뒤에 있는 서버는 시작시 m_serverAddrAtClient를 설정해야 함

            List<string> serverNicList = new List<String>();
            NetUtil.GetLocalIPAddresses(ref serverNicList);
            for (int i = 0; i < serverNicList.Count; ++i)
            {
                Console.WriteLine("Local IP : {0}", serverNicList[i]);
            }
            //sp.localNicAddr = serverNicList[0];

            Console.WriteLine("Public IP : {0}", _currentServerPublicIP);
            sp.serverAddrAtClient = _currentServerPublicIP;
            //sp.serverAddrAtClient = serverNicList[0];

            sp.SetExternalNetWorkerThreadPool(_networkerThreadPool);
            sp.SetExternalUserWorkerThreadPool(_userworkerThreadPool);

            _netServer.Start(sp);




            // DB에 로비 현황 기록을 위한 타이머 
            var findConfig = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
            if (findConfig.Success)
            {
                JObject onlyServerKnown = JObject.FromObject(findConfig.Value);

                int START_WAIT_TIME_MS = 0 * 1000;
                int PERIOD_TIME_MS = (int)onlyServerKnown["refreshLobbyStateTimeSec"] * 1000;
                System.Threading.TimerCallback timerDelegate = new System.Threading.TimerCallback(RefreshLobbyStateToDB);
                _refreshLobbyStateTimerReference = new System.Threading.Timer(timerDelegate, this, START_WAIT_TIME_MS, PERIOD_TIME_MS);

                CouchbaseManager.Instance.UpsertLobby(_currentServerGuid, _currentServerPublicIP, _netServer.GetClientHostIDs().Length, serverName);
            }
        }

        public void Dispose()
        {
            _netServer.Dispose();

            _refreshLobbyStateTimerReference.Dispose();
        }

        // private --------------------------------------------------------------------------------------------------------------------
        void RefreshLobbyStateToDB(object obj)
        {
            var result = CouchbaseManager.Instance.UpdateLobby(_currentServerGuid, _netServer.GetClientHostIDs().Length);
            if (result.Success)
            {
                // 순서 은근 중요하다 위에 Update보다 빠르게 되면 혹여나 이 함수 늦게 들어왔을 떄면 바로 날려버리기 때문에 Update에 의미가 없어지게 된다 
                RefreshLobbyServerDB();
            }

            //Console.WriteLine("Refresh Current Lobby State");
        }

        public static void RefreshLobbyServerDB()
        {
            var findConfig = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
            if (findConfig.Success)
            {
                // 서버는 1회용처럼 언제든 죽을 수 있기 때문에 갱신 주기가 너무 딜레이될 시 서버가 죽은걸로 판정하고 현황에서 삭제함
                JObject onlyServerKnown = JObject.FromObject(findConfig.Value);
                int removeWaitingLobbyStateTimeSec = (int)onlyServerKnown["removeWaitingLobbyStateTimeSec"];

                // DB에서 로비 현황 리스트 가져오기 
                var getLobbyListResult = CouchbaseManager.Instance.GetLobbyList();
                if (getLobbyListResult.Success)
                {
                    JObject resultRow = JObject.FromObject(getLobbyListResult.Rows[0]);
                    JArray lobbyList = JArray.FromObject(resultRow["lobbyList"]);
                    foreach (JObject lobby in lobbyList.Children<JObject>())
                    {
                        string id = (string)lobby["id"];
                        int userCount = (int)lobby["userCount"];
                        string ip = (string)lobby["ip"];

                        // system.formatexception : the string was not recognized as a valid datetime
                        // AWS EC2는 영어 기반이기 때문에 한글기반 윈도우랑 form이 차이가 나서 DateTime.Parse할 떄 예외가 던져진다
                        // 영어 한글 둘다 쓸 수 있는 공통 형식을 만들어줘야겠다 
                        DateTime refreshedTime = DateTime.ParseExact((string)lobby["refreshedTime"], SharedProject.Common.DateTimeForm, System.Globalization.CultureInfo.GetCultureInfo("ko-KR"));

                        // 이 DateTime.Now 하는 부분도 주의해야하는게 Parse한 refreshedTime이 자기 문화권이 아니라면
                        // 아마 UTC 기준으로 할 것이기 때문에 몇시간씩 차이가 날 것이다 
                        // 더 앞 시간이 될 수도 있고, 더 뒷 시간이 될 수도 있다
                        var interval = DateTime.Now - refreshedTime;

                        // 갱신 주기가 너무 오래됬다면 서버가 죽은걸로 판정하고 현황에서 삭제시킴
                        if (interval.TotalSeconds > removeWaitingLobbyStateTimeSec)
                        {
                            CouchbaseManager.Instance.RemoveLobby(id);

                            Console.WriteLine("Remove Lobby State. removedLobbyId : {0}", id);
                        }
                    }
                }
            }
        }

        void InitNetServer()
        {
            _netServer.AttachProxy(_S2CProxy);
            _netServer.AttachStub(_C2SStub);

            InitLobbyServer();

            InitBattleServer();

            _netServer.ConnectionRequestHandler = (AddrPort clientAddr, ByteArray userDataFromClient, ByteArray reply) =>
            {
                reply = new ByteArray();
                reply.Clear();

                // return false 시, 클라는 ErrorType_NotifyServerDeniedConnection을 받게된다 

                return true;
            };

            _netServer.ClientHackSuspectedHandler = (HostID clinetID, HackType hackType) =>
            {
                System.Threading.Monitor.Enter(this);

                // 로그 기록 
                WriteUserHackLog(clinetID, hackType.ToString());

                // 강제 Leave 처리
                LeaveUser(_netServer.GetClientInfo(clinetID), null, null);

                // 강제 연결 끊기 
                _netServer.CloseConnection(clinetID);

                System.Threading.Monitor.Exit(this);
            };

            _netServer.ClientJoinHandler = (NetClientInfo clientInfo) =>
            {
                Console.WriteLine("OnClientJoin: {0}", clientInfo.hostID);
            };

            _netServer.ClientLeaveHandler = (NetClientInfo clientInfo, ErrorInfo errorInfo, ByteArray comment) =>
            {
                Console.WriteLine("OnClientLeave: {0}", clientInfo.hostID);

                System.Threading.Monitor.Enter(this);

                LeaveUser(clientInfo, errorInfo, comment);

                System.Threading.Monitor.Exit(this);
            };

            _netServer.ErrorHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnError: {0}", errorInfo.ToString());
            };

            _netServer.WarningHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnWarning: {0}", errorInfo.ToString());
            };

            _netServer.ExceptionHandler = (Exception e) =>
            {
                Console.WriteLine("OnException: {0}", e.Message.ToString());
            };

            _netServer.InformationHandler = (ErrorInfo errorInfo) =>
            {
                Console.WriteLine("OnInformation: {0}", errorInfo.ToString());
            };

            _netServer.NoRmiProcessedHandler = (RmiID rmiID) =>
            {
                Console.WriteLine("OnNoRmiProcessed: {0}", rmiID);
            };

            _netServer.TickHandler = (object context) =>
            {

            };

            _netServer.UserWorkerThreadBeginHandler = () =>
            {
            };

            _netServer.UserWorkerThreadEndHandler = () =>
            {
            };
        }

        void WriteUserHackLog(HostID remote, string hackType)
        {
            RemoteUser hackRemoteUser;
            _remoteUsers.TryGetValue(remote, out hackRemoteUser);
            if (hackRemoteUser != null)
            {
                JObject userLogDB;
                JArray hackHistoryDB;

                var findUserLog = CouchbaseManager.Instance.FindUserLogWIthId(hackRemoteUser.userId);
                if (findUserLog.Success)
                {
                    userLogDB = JObject.FromObject(findUserLog.Value);

                    if (userLogDB["hackHistory"] != null)
                    {
                        hackHistoryDB = JArray.FromObject(userLogDB["hackHistory"]);
                    }
                    else
                    {
                        hackHistoryDB = new JArray();
                    }
                }
                else
                {
                    // 없으면 새로 생성하기
                    userLogDB = new JObject();
                    userLogDB["id"] = hackRemoteUser.userId;

                    hackHistoryDB = new JArray();
                }

                JObject newHackHistoryLog = new JObject();
                newHackHistoryLog["time"] = SharedProject.Common.CurrentTimeToString();
                newHackHistoryLog["hackType"] = hackType;
                hackHistoryDB.Add(newHackHistoryLog);

                userLogDB["hackHistory"] = hackHistoryDB;

                CouchbaseManager.Instance.UpsertUserLog(userLogDB);
            }
        }

        void InitGoogleAndroidPublisherService()
        {
            String serviceAccountEmail = "gunnersbattlecredential@api-project-64258587.iam.gserviceaccount.com";

            var certificate = new X509Certificate2(@"API Project--46c489d81d20.p12", "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = new[] { AndroidPublisherService.Scope.Androidpublisher }
            }.FromCertificate(certificate));

            _androidPublisherService = new AndroidPublisherService(new AndroidPublisherService.Initializer
            {
                ApplicationName = "gunnersbattlecredential",
                HttpClientInitializer = credential,
            });
        }

        void LeaveUser(NetClientInfo clientInfo, ErrorInfo errorInfo, ByteArray comment)
        {
            ClientLeave_LobbyServer(clientInfo, errorInfo, comment);

            ClientLeave_BattleServer(clientInfo, errorInfo, comment);

            // 로비, 배틀 처리가 다 되고나서 리모트 유저를 지운다 
            RemoteUser removeRemoteUser;
            _remoteUsers.TryRemove(clientInfo.hostID, out removeRemoteUser);
            if (removeRemoteUser != null)
            {
                // 리모트 유저 DB 갱신 
                var findResult = CouchbaseManager.Instance.FindUserWithId(removeRemoteUser.userId);
                if (findResult.Success)
                {
                    JObject user = JObject.FromObject(findResult.Value);

                    // 플레이 타임 갱신해서 DB에 넣어주기
                    removeRemoteUser.stopwatch.Stop();
                    uint stayTimeSec = (uint)removeRemoteUser.stopwatch.Elapsed.TotalSeconds;
                    user["playTimeSec"] = (uint)user["playTimeSec"] + stayTimeSec;

                    // 접속 처리 해제 
                    user["signed"] = false;

                    CouchbaseManager.Instance.UpdateUser(user);
                }
            }
        }

        void InitRMI()
        {
            InitRMI_LobbyServer();

            InitRMI_BattleServer();

            _C2SStub.JoinLobby = JoinLobby;
            _C2SStub.HackSuspected = HackSuspected;
        }

        bool JoinLobby(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            {
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("JoinLobby param : " + param);

                JObject result = new JObject();

                do
                {
                    // 접속량이 꽉 찼는지 확인  
                    var findConfig = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
                    if (findConfig.Success)
                    {
                        JObject onlyServerKnown = JObject.FromObject(findConfig.Value);

                        int lobbyServerUserLimit = (int)onlyServerKnown["lobbyServerUserLimit"];

                        // -1을 해주는 이유는 이미 이 로비에 접속된 순간부터 remote가 할당되기 때문에 동접에 추가되기 때문이다
                        // 정확한 계산을 위해 본인은 빼주고 계산해야한다 
                        if (_netServer.GetClientHostIDs().Length - 1 >= lobbyServerUserLimit)
                        {
                            result["result"] = "serverIsFull";

                            break;
                        }
                    }


                    // userId를 참조해서 userDB 가져오기 
                    string userId = (string)param["userId"];
                
                    var findResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (findResult.Success == false)
                    {
                        result["result"] = findResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(findResult.Value);


                    // 이미 접속되어있는 유저인지 체크
                    // remote로 검색하면 안되고 userId로 검색해야 함
                    // remote는 접속된 기기마다 다르게 배정되기 때문
                    bool alreadySignIn = user["signed"] == null ? false : (bool)user["signed"];
                    if (alreadySignIn)
                    {
                        // 동접을 차지하더라도 그냥 넘겨두자 로비마다 다르게 있긴 하더라도 각 디바이스별로 다르게 접속을 한 상태일테니 
                        // 이 부분은 DB 의존도가 커서 서버가 꺼진다거나 할 때 문제 생길 여지가 너무 많음 

                        //result["result"] = "alreadySignIn";

                        //break;
                    }


                    // 체크 통과했으면 접속 중이라고 DB에 써주기 
                    // DB에 기록하는 이유는 로비서버가 하나가 아닌 여러개가 될 수 있기 떄문
                    // 중복 접속을 막는 이유는 서버마다 독립적인 동작을 하고 있는데 각각의 서버에 계속 접속이 되어있을 수 있기 때문임
                    // 서버를 내가 직접 강종을 하지 않는 이상 문제 없이 동작할 것이다
                    // 로비가 꺼지기 찰나에 접속을 해버리면 문제가 될 수 있지만 그럴 경우는 드물꺼다 
                    user["signed"] = true;

                    CouchbaseManager.Instance.UpdateUser(user);

                    result["result"] = "success";
                    result["user"] = user;
                    result["lobbyP2PId"] = (int)_lobbyP2PId;


                    // 캐싱 데이터에 저장
                    RemoteUser remoteUser = new RemoteUser();
                    remoteUser.hostId = remote;
                    remoteUser.userId = userId;
                    remoteUser.stopwatch.Start();
                    _remoteUsers.TryAdd(remote, remoteUser);

                    // 이 때 로비에 조인시키는 것이다
                    ClientJoin_LobbyServer(remote);
                }
                while (false);
            
                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("JoinLobby result : " + jsonResult);

                _S2CProxy.NotifyJoinLobby(remote, RmiContext.ReliableSend, jsonResult);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool HackSuspected(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            {
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("HackSuspected param : " + param);

                // 로그 기록 
                WriteUserHackLog(remote, (string)param["hackType"]);

                // 강제 Leave 처리
                // 연결 끊기 전 다른 유저들을 위해 해줘야 함 
                LeaveUser(_netServer.GetClientInfo(remote), null, null);

                // 강제 연결 끊기 
                _netServer.CloseConnection(remote);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }
    }
}

