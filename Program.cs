﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ESC: Quit");

            LobbyServer server = new LobbyServer();

            // DB에서 확인할 서버 네임 세팅 
            if (args.Length > 0)
            {
                server.serverName = args[0];
            }

            try
            {
                server.Start();
                Console.WriteLine("Lobby Server start ok");
                
                while (true)
                {
                    if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.Message.ToString());
            }
            finally
            {
                server.Dispose();
            }
        }
    }
}
