﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Nettention.Proud;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GunnersBattleCommon;
using Google.Apis.AndroidPublisher.v2;
using CouchbaseShared;

namespace LobbyServer
{
    class Room
    {
        public HostID id = HostID.HostID_None;  // RoomId = P2PGroudId
        public GameMode mode = GameMode.AMBUSH;
        public Map map = Map.FOREST;
        public string title = "";
        public string password = "";
        public bool permissionParty = false;
        public bool isPlaying = false;
        public byte personCountForStart = 0;
        public bool vsAI = false;
        public ConcurrentDictionary<string, JObject> users = new ConcurrentDictionary<string, JObject>();

        public void Dispose()
        {
        }
    }

    public class RemoteUser
    {
        public HostID hostId = HostID.HostID_None;
        public HostID roomId = HostID.HostID_None;
        public string userId = "";
        public float loadingTime = 9999;    // 이 값 세팅 안되있는 유저는 아예 슈퍼피어가 안되버리게 9999로 해놓는다 
        public Stopwatch stopwatch = new Stopwatch();
    }

    partial class LobbyServer   // 이름만 일단 엔트리 서버
    {
        // 서버도 P2P 그룹에 추가시켜야하니 C2C 붙여야한다 

        // Key : P2PGroupID
        ConcurrentDictionary<HostID, Room> _rooms = new ConcurrentDictionary<HostID, Room>();

        // 유저가 서버와 연결이 끊겼을 경우, 빠르게 처리를 하기 위한 목적
        // 유저가 방에 들어갈 때 추가하고, 방에 빠져나올 때 삭제함
        // Key : HostID
        ConcurrentDictionary<HostID, RemoteUser> _remoteUsers = new ConcurrentDictionary<HostID, RemoteUser>();

        // 로비만의 P2P 
        HostID _lobbyP2PId = HostID.HostID_None;

        // public ---------------------------------------------------------------------------------------------------------------------
        void ClientJoin_LobbyServer(HostID remote)
        {
            // 로비만의 P2P 그룹을 만듬. 하지만 서버는 포함되지 않음 
            if (_lobbyP2PId == HostID.HostID_None)
            {
                // P2P를 만들 때 빈 P2P를 만들면 생성이 되질 않아서 접속한 유저가 한명 생기면 생성시킨다 
                HostID[] groupClientHostIds = { remote };
                _lobbyP2PId = _netServer.CreateP2PGroup(groupClientHostIds, new ByteArray()); 
            }

            // 로비 P2P 가입
            _netServer.JoinP2PGroup(remote, _lobbyP2PId);
        }

        void ClientLeave_LobbyServer(NetClientInfo clientInfo, ErrorInfo errorInfo, ByteArray comment)
        {
            ForceLeaveRoom(clientInfo.hostID);

            // 로비 P2P에서 나기기
            _netServer.LeaveP2PGroup(clientInfo.hostID, _lobbyP2PId);
        }

        void InitLobbyServer()
        {
            // P2P에 조인하고 다른 그룹원들에게 전부 통지가 완료된 후 들어옴 
            _netServer.P2PGroupJoinMemberAckCompleteHandler = (HostID groupHostID, HostID memberHostID, ErrorType result) =>
            {
                Console.WriteLine("P2PGroupJoinMemberAckCompleteHandler groupHostID : " + groupHostID + ", memberHostID : " + memberHostID);
            };
            
            // Proud.CNetServer.DestroyP2PGroup를 호출하거나 P2P group에 소속된 마지막 멤버가 서버와의 접속을 종료할 때 등
            _netServer.P2PGroupRemovedHandler = (HostID groupID) =>
            {
                Console.WriteLine("P2PGroupRemovedHandler groupID : " + groupID);
            };
        }

        void WriteUserGemLog(string userId, string where, int quantity, int currentGem, string productId = "", string transactionId = "")
        {
            JObject userLogDB;
            JArray gemHistoryDB;

            var findUserLog = CouchbaseManager.Instance.FindUserLogWIthId(userId);
            if (findUserLog.Success)
            {
                userLogDB = JObject.FromObject(findUserLog.Value);

                if (userLogDB["gemHistory"] != null)
                {
                    gemHistoryDB = JArray.FromObject(userLogDB["gemHistory"]);
                }
                else
                {
                    gemHistoryDB = new JArray();
                }
            }
            else
            {
                // 없으면 새로 생성하기
                userLogDB = new JObject();
                userLogDB["id"] = userId;

                gemHistoryDB = new JArray();
            }
            
            JObject newGemHistoryLog = new JObject();
            newGemHistoryLog["time"] = SharedProject.Common.CurrentTimeToString();
            newGemHistoryLog["where"] = where;
            newGemHistoryLog["quantity"] = quantity;
            newGemHistoryLog["current"] = currentGem;

            if (productId.Length > 0)
            {
                newGemHistoryLog["productId"] = productId;
            }

            if (transactionId.Length > 0)
            {
                newGemHistoryLog["transactionId"] = transactionId;
            }

            gemHistoryDB.Add(newGemHistoryLog);
            userLogDB["gemHistory"] = gemHistoryDB;

            CouchbaseManager.Instance.UpsertUserLog(userLogDB);
        }

        // private --------------------------------------------------------------------------------------------------------------------
        void ForceLeaveRoom(HostID remote)
        {
            RemoteUser leaveRemoteUser;
            _remoteUsers.TryGetValue(remote, out leaveRemoteUser);
            if (leaveRemoteUser != null)
            {
                Room leaveUserRoom;
                _rooms.TryGetValue(leaveRemoteUser.roomId, out leaveUserRoom);

                // 대기방만 처리. 플레이방은 배틀서버에서 처리
                if (leaveUserRoom != null && !leaveUserRoom.isPlaying)
                {
                    LeaveUserToRoom(leaveUserRoom, leaveRemoteUser.userId, leaveRemoteUser.hostId);
                }
            }
        }

        void LeaveUserToRoom(Room room, string userId, HostID userHostId)
        {
            // 넷서버에 P2P Leave
            _netServer.LeaveP2PGroup(userHostId, room.id);

            // 룸 유저 리스트에서 빼기
            JObject removeUser;
            room.users.TryRemove(userId, out removeUser);

            // 유저가 한명도 없을 경우 방 삭제 
            if (room.users.IsEmpty)
            {
                Room removeRoom;
                _rooms.TryRemove(room.id, out removeRoom);
                removeRoom.Dispose();

                // P2P 그룹 해제
                _netServer.DestroyP2PGroup(room.id);
            }
            else
            {
                // 이미 룸안에 접속해있는 그룹원들에게 통지
                JObject leaveParam = new JObject();
                leaveParam["groupId"] = (int)room.id;
                leaveParam["leaveUserId"] = userId;

                string leaveJSONParam = JsonConvert.SerializeObject(leaveParam);
                _C2CProxy.Leave(room.id, RmiContext.ReliableSend, leaveJSONParam);
            }

            // 리모트 유저 roomId 리셋
            RemoteUser leaveRemoteUser;
            _remoteUsers.TryGetValue(userHostId, out leaveRemoteUser);
            if (leaveRemoteUser != null)
            {
                leaveRemoteUser.roomId = HostID.HostID_None;
            }
        }

        void JoinRoom(HostID remote, JObject user, string userId, Room room)
        {
            // 넷서버에 P2P Join
            _netServer.JoinP2PGroup(remote, room.id);

            // 룸 유저 리스트에 추가 
            JObject roomUser = new JObject();
            roomUser["ready"] = false;
            roomUser["hostId"] = (int)remote;
            roomUser["user"] = user;
            roomUser["ping"] = _netServer.GetLastUnreliablePingMs(remote);
            room.users.TryAdd(userId, roomUser);

            // 리모트유저에 roomId 저장
            RemoteUser remoteUser;
            _remoteUsers.TryGetValue(remote, out remoteUser);
            if (remoteUser != null)
            {
                remoteUser.roomId = room.id;
            }
            // 빠른 테스트를 위해 SignIn 재끼고 오는 경우 없을 수 있다 
            else
            {
                RemoteUser newRemoteUser = new RemoteUser();
                newRemoteUser.hostId = remote;
                newRemoteUser.userId = user["id"].ToObject<string>();
                newRemoteUser.roomId = room.id;
                newRemoteUser.stopwatch.Start();
                _remoteUsers.TryAdd(remote, newRemoteUser);
            }

            // 이미 룸안에 접속해있는 그룹원들에게 통지
            JObject joinParam = new JObject();
            joinParam["groupId"] = (int)room.id;
            joinParam["joinUser"] = roomUser;
            string joinJSONParam = JsonConvert.SerializeObject(joinParam);
            _C2CProxy.Join(room.id, RmiContext.ReliableSend, joinJSONParam);
        }

        // RMI ------------------------------------------------------------------------------------------------------------------------
        void InitRMI_LobbyServer()
        {
            _C2SStub.CreateRoom = CreateRoom;
            _C2SStub.JoinRoom = JoinRoom;
            _C2SStub.LeaveRoom = LeaveRoom;
            _C2SStub.RequestRoomList = RequestRoomList;
            _C2SStub.FastStart = FastStart;

            _C2SStub.Megaphone = Megaphone;

            _C2SStub.Buy = Buy;
            _C2SStub.Use = Use;

            _C2SStub.LevelClear = LevelClear;
            _C2SStub.WatchVideo = WatchVideo;
        }

        bool CreateRoom(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("CreateRoom param : " + param);

                JObject result = new JObject();

                // 존재하는 유저인지 체크 
                var userFindResult = CouchbaseManager.Instance.FindUserWithId(param["userId"].ToObject<string>());
                if (userFindResult.Success)
                {
                    JObject user = JObject.FromObject(userFindResult.Value);

                    // 새로운 P2P 그룹 생성 
                    //HostID[] groupClientHostIds = { remote, HostID.HostID_Server };
                    HostID[] groupClientHostIds = { remote };
                    HostID newP2PGroupId = _netServer.CreateP2PGroup(groupClientHostIds, new ByteArray());

                    // 그룹 생성되면 룸 하나 생성 param으로 넘어온 값 찾아서 세팅 
                    Room newRoom = new Room();
                    newRoom.id = newP2PGroupId;
                    newRoom.map = param["map"].ToObject<Map>();
                    newRoom.mode = param["mode"].ToObject<GameMode>();
                    newRoom.permissionParty = param["permissionParty"].ToObject<bool>();
                    newRoom.title = param["title"].ToObject<string>();
                    newRoom.password = param["password"].ToObject<string>();
                    newRoom.vsAI = param["vsAI"].ToObject<bool>();

                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);
                    newRoom.personCountForStart = (byte)config["battleUserLimit"];
                    // vsAI라면, 허용 유저 수를 반으로 줄인다 
                    if (newRoom.vsAI)
                    {
                        newRoom.personCountForStart /= 2;
                    }
                    //newRoom.personCountForStart = param["personCountForStart"].ToObject<byte>();

                    JObject roomUser = new JObject();
                    roomUser["ready"] = false;
                    roomUser["hostId"] = (int)remote;
                    roomUser["user"] = user;
                    roomUser["ping"] = _netServer.GetLastUnreliablePingMs(remote);
                    newRoom.users.TryAdd(user["id"].ToObject<string>(), roomUser);

                
                    // 방 리스트에 그룹ID를 키로 TryAdd
                    _rooms.TryAdd(newP2PGroupId, newRoom);

                    // 리모트유저에 roomId 저장
                    RemoteUser remoteUser;
                    _remoteUsers.TryGetValue(remote, out remoteUser);
                    if (remoteUser != null)
                    {
                        remoteUser.roomId = newP2PGroupId;
                    }
                    // 빠른 테스트를 위해 SignIn 재끼고 오는 경우 없을 수 있다 
                    else
                    {
                        RemoteUser newRemoteUser = new RemoteUser();
                        newRemoteUser.hostId = remote;
                        newRemoteUser.userId = user["id"].ToObject<string>();
                        newRemoteUser.roomId = newP2PGroupId;
                        newRemoteUser.stopwatch.Start();
                        _remoteUsers.TryAdd(remote, newRemoteUser);
                    }

                    // 결과는 방 정보를 그대로 보내주기 
                    result["result"] = "success";
                    result["room"] = JObject.FromObject(newRoom);
                }
                else
                {
                    result["result"] = userFindResult.Message;
                }

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("CreateRoom result : " + jsonResult);

                _S2CProxy.NotifyCreateRoom(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool JoinRoom(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            {
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("JoinRoom param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // 해당 ID를 갖는 룸 찾기
                    HostID roomId = (HostID)param["roomId"].ToObject<int>();
                    Room room;
                    _rooms.TryGetValue(roomId, out room);
                    if (room == null)
                    {
                        result["result"] = "notFoundRoom";

                        break;
                    }

                    // 시작방이면 안됨
                    if (room.isPlaying)
                    {
                        result["result"] = "alreadyStarted";

                        break;
                    }

                    // 패스워드 체크
                    string password = param["password"].ToObject<string>();
                    if (room.password != password)
                    {
                        result["result"] = "notCorrespondPassword";

                        break;
                    }

                    // 인원수 체크 
                    if (room.users.Count >= room.personCountForStart)
                    {
                        result["result"] = "roomIsFull";

                        break;
                    }



                    JoinRoom(remote, user, userId, room);

                    // 룸 정보를 결과로 보냄 
                    result["result"] = "success";
                    result["room"] = JObject.FromObject(room);


                } while (false);

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("CreateRoom result : " + jsonResult);

                _S2CProxy.NotifyJoinRoom(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool LeaveRoom(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("LeaveRoom param : " + param);

                JObject result = new JObject();

                // 존재하는 유저인지 체크 
                string userId = param["userId"].ToObject<string>();
                var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                if (userFindResult.Success)
                {
                    // 해당 ID를 갖는 룸 찾기
                    HostID roomId = (HostID)param["roomId"].ToObject<int>();
                    Room room;
                    _rooms.TryGetValue(roomId, out room);
                    if (room != null)
                    {
                        LeaveUserToRoom(room, userId, remote);

                        // 결과만을 보냄
                        result["result"] = "success";
                    }
                    else
                    {
                        result["result"] = "notFoundRoom";
                    }
                }
                else
                {
                    result["result"] = userFindResult.Message;
                }

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("LeaveRoom result : " + jsonResult);

                _S2CProxy.NotifyLeaveRoom(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool RequestRoomList(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("RequestRoomList param : " + param);

                // 복사
                ConcurrentDictionary<HostID, Room> rooms = _rooms;
                /*
                ConcurrentDictionary<HostID, Room> rooms = new ConcurrentDictionary<HostID, Room>(_rooms);

                // 플레이 중인 방 빼기 
                foreach (var room in _rooms)    // 우회전법
                {
                    if (room.Value.isPlaying)
                    {
                        Room removeRoom;
                        rooms.TryRemove(room.Key, out removeRoom);
                    }
                }*/

                JObject result = new JObject();
                result["result"] = "success";
                result["rooms"] = JObject.FromObject(rooms);

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("RequestRoomList result : " + jsonResult);

                _S2CProxy.NotifyRequestRoomList(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool FastStart(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("FastStart param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // 방 랜덤 찾기 
                    ConcurrentDictionary<HostID, Room> rooms = new ConcurrentDictionary<HostID, Room>(_rooms);

                    foreach (var copyroom in _rooms)    // 우회전법
                    {
                        // 비밀번호 방 빼기 
                        // 플레이 중인 방 빼기 
                        // 꽉 찬 방 빼기 
                        if (copyroom.Value.password != "" || copyroom.Value.isPlaying || copyroom.Value.users.Count >= copyroom.Value.personCountForStart)
                        {
                            Room removeRoom;
                            rooms.TryRemove(copyroom.Key, out removeRoom);
                        }
                    }

                    var roomsList = rooms.Values.ToList();
                    if (roomsList.Count == 0)
                    {
                        result["result"] = "notFoundRoom";

                        break;
                    }

                    Random random = new Random();
                    int randomIndex = random.Next(0, roomsList.Count);
                    Room room = roomsList[randomIndex];

                    JoinRoom(remote, user, userId, room);

                    // 룸 정보를 결과로 보냄 
                    result["result"] = "success";
                    result["room"] = JObject.FromObject(room);
                }
                while (false);

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("FastStart result : " + jsonResult);

                _S2CProxy.NotifyFastStart(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool Megaphone(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Megaphone param : " + param);

                JObject result = new JObject();

                // 메가폰 사용이 가능하다면 로비 전체가 가입되있는 P2P에 통지한다
                // 그렇지 않다면 사용한 유저에게만 실패 결과를 보낸다 
                HostID toRemote = HostID.HostID_None;

                // 존재하는 유저인지 체크 
                string userId = param["userId"].ToObject<string>();
                var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                if (userFindResult.Success)
                {
                    JObject user = JObject.FromObject(userFindResult.Value);
                
                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);
                    int MEGAPHONE_USE_GEM = config["megaphone_gem"].ToObject<int>();

                    // 보유 잼 체크 
                    if ((int)user["gem"] >= MEGAPHONE_USE_GEM)
                    {
                        toRemote = _lobbyP2PId;

                        user["gem"] = (int)user["gem"] - MEGAPHONE_USE_GEM;

                        // DB 갱신
                        CouchbaseManager.Instance.UpdateUser(user);

                        // 잼 로그 기록
                        // [시간, 증감된 곳, 증감량, 갱신된 량] 
                        WriteUserGemLog(userId, "megaphone", -MEGAPHONE_USE_GEM, (int)user["gem"]);

                        // 통지
                        result["result"] = "success";
                        result["userId"] = user["id"];
                        result["nickname"] = user["nickname"];
                        result["gem"] = user["gem"];
                        result["megaphone"] = param["megaphone"].ToObject<string>();
                    }
                    else
                    {
                        toRemote = remote;

                        result["result"] = "notEnoughGem";
                        result["gem"] = user["gem"];
                    }
                }
                else
                {
                    toRemote = remote;

                    result["result"] = userFindResult.Message;
                }

                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("Megaphone result : " + jsonResult);

                _S2CProxy.NotifyMegaphone(toRemote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool Buy(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Buy param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);

                    // type 체크 
                    ProductType type = (GunnersBattleCommon.ProductType)Enum.Parse(typeof(GunnersBattleCommon.ProductType), (string)param["type"]);
                    if (type == ProductType.Gem)
                    {
                        // 영수증 검증 절차
                        string productId = (string)param["productId"];
                        string transactionId = (string)param["transactionId"];

                        string store = (string)param["store"];
                        if (store == "google")
                        {
                            string packageName = (string)param["packageName"];
                            string token = (string)param["token"];

                            // DB 중복 영수증 체크 
                            if (!CouchbaseManager.Instance.CheckGoogleReceipt(token))
                            {
                                result["result"] = "productDuplication";

                                break;
                            }

                            // 좀 우려되는게 하루 200,000 호출이기 때문에 이를 넘어서면 어떻게 결과가 떨어질지 모르겠음
                            // 구글에 신청해서 더 늘릴 수 있긴한데 말이지 
                            Google.Apis.AndroidPublisher.v2.Data.ProductPurchase receiptValidationResult = null;
                            try
                            {
                                receiptValidationResult = _androidPublisherService.Purchases.Products.Get(packageName, productId, token).Execute();
                            }
                            catch (Google.GoogleApiException exception)
                            {
                                result["result"] = exception.Error.Message;

                                break;
                            }

                            if (receiptValidationResult == null)
                            {
                                // 위에 try-catch를 거쳤는데도 result에 null이 들어간거라면... 정상 실행됬는데도 null이 됬다는것임
                                result["result"] = "productCheckNull";

                                break;
                            }

                            // 0 정상 구매
                            // 1 구매 취소 (근데 이게 취소가 됬다해도 반영이 바로 안 되는 경우가 있다고 하니 중복 영수증 검증은 필수다)
                            if (receiptValidationResult.PurchaseState != 0)
                            {
                                result["result"] = "produtcCancelled";

                                break;
                            }

                            // receiptValidationResult.ConsumptionState
                            // 0 구매 완료 처리 안됨
                            // 1 구매 완료 처리됨

                            // Ultimate Mobile 내부에서 V3 IAP 방식을 쓰지 않는 듯하다 
                            // 구매하면 바로 구매 완료처리가 되버리는 듯 
                            // 구매 완료처리되있는 것도 결제 성공이라 간주하자

                            // 영수증 DB에 기록
                            CouchbaseManager.Instance.PutGoogleReceipt(token);
                        }
                        else if (store == "apple")
                        {

                        }

                        // 잼 이름은 IAP id로 넘어올 것이니 config에서 찾는다 
                        int amount = (int)config[productId];

                        // 수량 증가 
                        user["gem"] = (int)user["gem"] + amount;

                        // DB 갱신
                        CouchbaseManager.Instance.UpdateUser(user);

                        // 잼 로그
                        // transactionId가 영수증 검증에 직접적으로 필요하진 않지만 메일로 들어오는 영수증 주문번호가 transactionId이기 때문에 확인할 때 쓸 수 있다
                        WriteUserGemLog(userId, "buy_gem", amount, (int)user["gem"], productId, transactionId);

                        // 결과
                        result["result"] = "success";
                        result["user"] = user;

                        // 완료 처리를 위한 정보 보내주기 
                        result["productId"] = productId;
                        result["transactionId"] = transactionId;
                    }
                    else if (type == ProductType.Etcetera)
                    {
                        string name = (string)param["name"];

                        // 보유 잼 체크 
                        int gemPrice = (int)config[type.ToString()][name];
                        if ((int)user["gem"] < gemPrice)
                        {
                            result["result"] = "notEnoughGem";
                            result["gem"] = user["gem"];

                            break;
                        }

                        // 닉네임 변경 아이템
                        if (name == "ChangeNickname")
                        {
                            // 중복 닉네임 체크 
                            // 없는 유저인지 DB에서 닉네임 검색
                            string nickname = (string)param["nickname"];
                            var findResult = CouchbaseManager.Instance.FindUserWithField("nickname", nickname);
                            if (findResult.Success)
                            {
                                result["result"] = "existUser";

                                break;
                            }

                            // DB에 절대 없는 유저여야만 함
                            if (findResult.Message != "notFoundUser")
                            {
                                result["result"] = findResult.Message;

                                break;
                            }

                            // 체크를 전부 통과했다면 닉네임 변경 시키기
                            user["nickname"] = nickname;
                        }
                        // 골드 환전 아이템
                        else if (name == "ExchangeToGold")
                        {
                            // 골드 증가 시켜주기 
                            int exchangeToGoldAmount = (int)config["exchangeToGoldAmount"];

                            // 골드 시스템은 이후에 들어가는 컨텐츠라 기존 유저 DB에 추가 시켜줘야 함 
                            if (user["gold"] == null)
                            {
                                user["gold"] = 0;
                            }

                            user["gold"] = (int)user["gold"] + exchangeToGoldAmount;
                        }
                        // 킬데스 초기화
                        else if (name == "KillDeathInitialization")
                        {
                            user["kill"] = 0;
                            user["death"] = 0;
                        }
                        // 전적 초기화
                        else if (name == "BattleRecordInitialization")
                        {
                            user["win"] = 0;
                            user["lose"] = 0;
                            user["draw"] = 0;
                        }

                        // 가격만큼 잼 차감 
                        user["gem"] = (int)user["gem"] - gemPrice;

                        // DB 갱신
                        CouchbaseManager.Instance.UpdateUser(user);

                        // 잼 로그 기록
                        WriteUserGemLog(userId, "buy_" + name, -gemPrice, (int)user["gem"]);

                        // 결과
                        result["result"] = "success";
                        result["user"] = user;
                    }
                    else 
                    {
                        string name = (string)param["name"];

                        // 이펙트 아이템은 이후에 들어가는거라 기존 유저 DB에 추가 시켜줘야 함 
                        if (type == ProductType.Effect && user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()] == null)
                        {
                            user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()] = new JObject();
                            user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()]["BAM"] = new JObject();
                        }

                        // 인벤토리에 이미 있는건지 체크 
                        bool isExisting = user["inventory"][type.ToString()][name] != null ? true : false;
                        if (isExisting)
                        {
                            result["result"] = "alreadyExist";

                            break;
                        }

                        // 골드 시스템은 이후에 들어가는 컨텐츠라 기존 유저 DB에 추가 시켜줘야 함 
                        if (user["gold"] == null)
                        {
                            user["gold"] = 0;
                        }

                        // 보유 골드 체크 
                        int goldPrice = (int)config[type.ToString()][name];
                        if ((int)user["gold"] < goldPrice)
                        {
                            result["result"] = "notEnoughGold";
                            result["gold"] = user["gold"];

                            break;
                        }

                        // 가격만큼 골드 차감 
                        user["gold"] = (int)user["gold"] - goldPrice;

                        // 인벤토리에 반영시켜주기 
                        user["inventory"][type.ToString()][name] = new JObject();

                        // DB 갱신
                        CouchbaseManager.Instance.UpdateUser(user);

                        // 골드 기록은 남기지 않는다 금방 쌓일 것 같음 

                        // 결과
                        result["result"] = "success";
                        result["user"] = user;
                    }

                } while (false);
                
                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("Buy result : " + jsonResult);

                _S2CProxy.NotifyBuy(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool Use(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Use param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // type 체크 
                    GunnersBattleCommon.ProductType type = (GunnersBattleCommon.ProductType)Enum.Parse(typeof(GunnersBattleCommon.ProductType), (string)param["type"]);
                    string name = (string)param["name"];

                    // 이펙트 아이템은 이후에 들어가는거라 기존 유저 DB에 추가 시켜줘야 함 
                    if (type == ProductType.Effect && user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()] == null)
                    {
                        user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()] = new JObject();
                        user["inventory"][GunnersBattleCommon.ProductType.Effect.ToString()]["BAM"] = new JObject();
                    }

                    if (type == ProductType.Chacracter || type == ProductType.Weapon || type == ProductType.Effect)
                    {
                        // 인벤토리에 이미 있는건지 체크 
                        bool isExisting = user["inventory"][type.ToString()][name] != null;
                        if (!isExisting)
                        {
                            result["result"] = "notExist";

                            break;
                        }

                        // 인벤토리에 반영시켜주기 
                        if (type == ProductType.Chacracter)
                        {
                            user["character"] = name;
                        }
                        else if (type == ProductType.Weapon)
                        {
                            user["weapon"] = name;
                        }
                        else if (type == ProductType.Effect)
                        {
                            user["effect"] = name;
                        }

                        // DB 갱신
                        CouchbaseManager.Instance.UpdateUser(user);

                        // 결과
                        result["result"] = "success";
                        result["user"] = user;
                    }

                } while (false);


                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("Use result : " + jsonResult);

                _S2CProxy.NotifyUse(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool LevelClear(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            {
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("LevelClear param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // 이후에 추가되는 시스템이라 null이면 넣어주기
                    if(user["levels"] == null)
                    {
                        user["levels"] = new JObject();
                    }

                    string clearLevel = (string)param["level"];
                    byte stars = (byte)param["stars"];

                    // 처음 클리어한 경우
                    if (user["levels"][clearLevel] == null)
                    {
                        user["levels"][clearLevel] = stars;
                    }
                    // 이미 깬 레벨일 경우
                    else
                    {
                        // 기존 별보다 높은 경우 갱신
                        if (stars > (byte)user["levels"][clearLevel])
                        {
                            user["levels"][clearLevel] = stars;
                        }
                    }

                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);

                    // 레벨 클리어 보상 반영                    
                    if (user["gold"] == null)
                    {
                        user["gold"] = 0;
                    }
                    int reward = config["levels"][clearLevel] != null ? (int)config["levels"][clearLevel] : 0;
                    user["gold"] = (int)user["gold"] + reward;

                    // EXP 반영
                    float levelExpRatio = (float)config["levelExpRatio"];
                    int rewardExp = (int)(reward * levelExpRatio);
                    if (user["exp"] == null)
                    {
                        user["exp"] = 0;
                    }
                    user["exp"] = (int)user["exp"] + rewardExp;

                    // 랭크 업데이트
                    // 경험치 반영되고 해줘야 한다 
                    UpdateUserRank(user);

                    // DB 갱신
                    CouchbaseManager.Instance.UpdateUser(user);

                    // 결과
                    result["result"] = "success";
                    result["user"] = user;
                    result["earn"] = reward;
                    result["earnExp"] = rewardExp;

                } while (false);


                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("LevelClear result : " + jsonResult);

                _S2CProxy.NotifyLevelClear(remote, RmiContext.ReliableSend, jsonResult);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool WatchVideo(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            {
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Use param : " + param);

                JObject result = new JObject();

                do
                {
                    // 존재하는 유저인지 체크 
                    string userId = param["userId"].ToObject<string>();
                    var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
                    if (!userFindResult.Success)
                    {
                        result["result"] = userFindResult.Message;

                        break;
                    }

                    JObject user = JObject.FromObject(userFindResult.Value);

                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);

                    // 비디오를 한번이라도 본 상태라면
                    if (user["watchVideoTime"] != null)
                    {
                        DateTime watchVideoTime = DateTime.ParseExact((string)user["watchVideoTime"], SharedProject.Common.DateTimeForm, System.Globalization.CultureInfo.GetCultureInfo("ko-KR"));
                        
                        // 현재 시간이 쿨타임 종료 시간 전이라면 더 기다려야 함 
                        if ((DateTime.Now - watchVideoTime).TotalSeconds < (double)config["watchVideoCoolTimeSec"])
                        {
                            result["result"] = "watchingCoolTime";
                            result["waitingTime"] = (DateTime.Now - watchVideoTime).TotalSeconds;

                            break;
                        }
                    }

                    // 보상 지급
                    int watchVideoReward = (int)config["watchVideoReward"];
                    if (user["gold"] == null)
                    {
                        user["gold"] = 0;
                    }
                    user["gold"] = (int)user["gold"] + watchVideoReward;

                    // 시간 갱신 
                    user["watchVideoTime"] = SharedProject.Common.CurrentTimeToString();

                    // DB 갱신
                    CouchbaseManager.Instance.UpdateUser(user);

                    // 결과
                    result["result"] = "success";
                    result["user"] = user;
                    result["earn"] = watchVideoReward;

                } while (false);


                string jsonResult = JsonConvert.SerializeObject(result);
                Console.WriteLine("WatchVideo result : " + jsonResult);

                _S2CProxy.NotifyWatchVideo(remote, RmiContext.ReliableSend, jsonResult);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }
    }
}
