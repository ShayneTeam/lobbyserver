﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Nettention.Proud;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GunnersBattleCommon;
using CouchbaseShared;

namespace LobbyServer
{
    public class Battle
    {
        public HostID groupId = HostID.HostID_None;
        public GameMode mode = GameMode.AMBUSH;
        public Map map = Map.FOREST;
        public byte personCountForStart = 0;
        public bool vsAI = false;
        public ConcurrentDictionary<string, JObject> users = new ConcurrentDictionary<string, JObject>();
        public RemoteUser superPeer = null;
        public Stopwatch stopwatch = new Stopwatch();

        public System.Threading.Timer superPeerCheckTimerReference;
        public Stopwatch stopwatchForSuperPeerCheck = new Stopwatch();

        public void Dispose()
        {
            superPeerCheckTimerReference.Dispose();
        }
    }

    partial class LobbyServer   // 이름만 일단 엔트리 서버
    {
        // 서버 C2C는 하나의 그룹만 관리하지 않는다는 것을 유의

        BattleC2C.Proxy _C2CProxy = new BattleC2C.Proxy();
        BattleC2C.Stub _C2CStub = new BattleC2C.Stub();

        // 실제로 시작한 배틀에서 들어온 결과인지 확인해야 함 
        // Key : roomId
        ConcurrentDictionary<HostID, Battle> _battles = new ConcurrentDictionary<HostID, Battle>();

        // public ---------------------------------------------------------------------------------------------------------------------
        void InitBattleServer()
        {
            _netServer.AttachProxy(_C2CProxy);
            _netServer.AttachStub(_C2CStub);
        }

        void ClientLeave_BattleServer(NetClientInfo clientInfo, ErrorInfo errorInfo, ByteArray comment)
        {
            ForceLeaveBattleAndRoom(clientInfo.hostID);
        }

        // private --------------------------------------------------------------------------------------------------------------------
        void UpdateUserRank(JObject userDB)
        {
            var findConfig = CouchbaseManager.Instance.FindConfig("static");
            if (findConfig.Success)
            {
                JObject config = JObject.FromObject(findConfig.Value);
                JArray ranks = JArray.FromObject(config["ranks"]);
                foreach (JObject rank in ranks.Children<JObject>())
                {
                    if (userDB["exp"] == null)
                    {
                        userDB["exp"] = 0;
                    }
                    if ((int)userDB["exp"] >= (int)rank["exp"])
                    {
                        userDB["rank"] = rank["rank"];

                        break;
                    }
                }
            }
        }

        void UpdateUserAfterBattle(Room room, string userId, bool win, bool lose, bool draw, int kill, int death, int rewardGold, int exp)
        {
            var userFindResult = CouchbaseManager.Instance.FindUserWithId(userId);
            if (userFindResult.Success && room != null)
            {
                JObject userDB = JObject.FromObject(userFindResult.Value);

                // kill / death 기록 
                userDB["kill"] = (int)userDB["kill"] + kill;
                userDB["death"] = (int)userDB["death"] + death;

                if (win)
                {
                    // 승리
                    userDB["win"] = (int)userDB["win"] + 1;
                }
                else if (lose)
                {
                    // 패배
                    userDB["lose"] = (int)userDB["lose"] + 1;
                }
                else if (draw)
                {
                    // 무승부
                    userDB["draw"] = (int)userDB["draw"] + 1;
                }

                // 유저 DB에 보상 지급
                if (userDB["gold"] == null)
                {
                    userDB["gold"] = 0;
                }
                userDB["gold"] = (int)userDB["gold"] + rewardGold;

                // 경험치 반영
                if (userDB["exp"] == null)
                {
                    userDB["exp"] = 0;
                }
                userDB["exp"] = (int)userDB["exp"] + exp;

                // 랭크 업데이트
                // 경험치 반영되고 해줘야 한다 
                UpdateUserRank(userDB);

                // 룸 유저 갱신
                JObject roomUser;
                room.users.TryGetValue(userId, out roomUser);
                if (roomUser != null)
                {
                    roomUser["user"] = userDB;
                }

                // DB에 유저 갱신
                CouchbaseManager.Instance.UpdateUser(userDB);
            }
        }

        void ChnageSuperPeer(Battle battle)
        {
            HostID newSuperPeerId;
            if (battle.superPeer != null)
            {
                // 이전 수퍼피어는 빼고 새로운 수퍼피어를 찾아야한다 
                SuperPeerSelectionPolicy superPeerSelectionPolicy = SuperPeerSelectionPolicy.GetOrdinary();
                newSuperPeerId = GetMostSuitableSuperPeerInGroupByLoadingTime(battle.groupId, battle.superPeer.hostId);
            }
            else
            {
                newSuperPeerId = GetMostSuitableSuperPeerInGroupByLoadingTime(battle.groupId);
            }
                    
            RemoteUser newSuperPeerRemoteUser;
            _remoteUsers.TryGetValue(newSuperPeerId, out newSuperPeerRemoteUser);

            if (newSuperPeerRemoteUser != null)
            {
                // 간혹 superPeer에 NULL이 들어가버리는 것을 방지 
                // 어차피 배틀에서 주기적으로 타이머를 돌려서 수퍼피어 변경을 체크하기 떄문에 괜찮을 것 같음 
                battle.superPeer = newSuperPeerRemoteUser;

                JObject selectSuperPeerParam = new JObject();
                selectSuperPeerParam["groupId"] = (int)battle.groupId;
                selectSuperPeerParam["superPeer"] = JObject.FromObject(newSuperPeerRemoteUser);

                string json = JsonConvert.SerializeObject(selectSuperPeerParam);
                Console.WriteLine("ChnageSuperPeer : " + json);
                _C2CProxy.SelectSuperPeer(battle.groupId, RmiContext.ReliableSend, json);
            }
        }

        void CheckSuperPeer(object battleObj)
        {
            Battle battle = (Battle)battleObj;

            // config 
            var findConfig = CouchbaseManager.Instance.FindConfig("static");
            JObject config = JObject.FromObject(findConfig.Value);

            int SUPER_PEER_CHANGE_TIME_MS = (int)config["checkSuperPeerChangeTimeMs"];

            // 수퍼피어가 마지막으로 보내온 핑 트래픽 시간 
            uint superPeerPingIntervalTimeMs = (uint)battle.stopwatchForSuperPeerCheck.Elapsed.TotalMilliseconds;

            // 수퍼피어가 핑을 보내온 시간이 기준 시간을 초과했다면 수퍼피어를 변경한다 
            if (superPeerPingIntervalTimeMs >= SUPER_PEER_CHANGE_TIME_MS)
            {
                ChnageSuperPeer(battle);
            }
        }

        bool ForceLeaveBattleAndRoom(HostID remote)
        {
            RemoteUser leaveRemoteUser;
            _remoteUsers.TryGetValue(remote, out leaveRemoteUser);
            if (leaveRemoteUser != null)
            {
                Room leaveUserRoom;
                _rooms.TryGetValue(leaveRemoteUser.roomId, out leaveUserRoom);

                Battle battle;
                _battles.TryGetValue(leaveRemoteUser.roomId, out battle);

                // 플레이중인 방 처리
                if (leaveUserRoom != null && leaveUserRoom.isPlaying && battle != null)
                {
                    // 배틀에서 해당 유저 삭제
                    JObject removeBattleUser;
                    battle.users.TryRemove(leaveRemoteUser.userId, out removeBattleUser);

                    // 유저가 한명도 없는 배틀은 삭제
                    // AI 대전 방이었다면 AI가 포함된 유저 수에서 AI 할당 갯수를 빼고 체크해야 함
                    if (battle.users.IsEmpty || (battle.vsAI && (battle.users.Count - battle.personCountForStart / 2) <= 0))
                    {
                        Battle removeBattle;
                        _battles.TryRemove(leaveUserRoom.id, out removeBattle);
                        removeBattle.Dispose();
                    }
                    // 유저가 한명이라도 있다면
                    else
                    {
                        // Leave된 애가 수퍼피어였다면 새로운 수퍼피어 선정
                        if (battle.superPeer == null || battle.superPeer.hostId == remote)
                        {
                            ChnageSuperPeer(battle);
                        }

                        // 패널티 처리
                        var userFindResult = CouchbaseManager.Instance.FindUserWithId(leaveRemoteUser.userId);
                        JObject userDB = JObject.FromObject(userFindResult.Value);
                        if (userFindResult.Success)
                        {
                            // 패배 기록
                            userDB["lose"] = (int)userDB["lose"] + 1;

                            CouchbaseManager.Instance.UpdateUser(userDB);
                        }
                    }

                    // 방나가기
                    LeaveUserToRoom(leaveUserRoom, leaveRemoteUser.userId, leaveRemoteUser.hostId);

                    return true;
                }
            }

            return false;
        }

        HostID GetMostSuitableSuperPeerInGroupByLoadingTime(HostID groupId, HostID excludee = HostID.HostID_None)
        {
            Dictionary<HostID, float> membersLoadingTime = new Dictionary<HostID, float>();

            P2PGroup p2pGroupInfo = _netServer.GetP2PGroupInfo(groupId);
            var members = p2pGroupInfo.members;
            // 멤버가 혼자인데 수퍼피어 체인지가 들어왔다면 excludee가 본인으로 들어와서 리스트카운팅이 0이라 예외가 생긴다
            // 멤버가 혼자면 그냥 본인이 수퍼피어 되어야 함 
            if (members.GetCount() == 1)
            {
                return members.At(0);
            }
            else if (members.GetCount() == 0)
            {
                return HostID.HostID_None;
            }

            for (int i = 0; i < members.GetCount(); ++i)
            {
                HostID memberHostId = members.At(i);

                // 제외해야 할 멤버라면 제외시키기 
                if (memberHostId == excludee)
                {
                    continue;
                }

                RemoteUser memberRemoteUser;
                _remoteUsers.TryGetValue(memberHostId, out memberRemoteUser);
                membersLoadingTime.Add(memberHostId, memberRemoteUser.loadingTime);
            }
            // 로딩타임 기준으로 오름차순 정렬 때리고
            var membersOrderByLoadingTime = membersLoadingTime.OrderBy(num => num.Value);

            // 최근 핑 타임이 정말 느리지 않다면 써주도록 한다
            const int PING_TIME_MS_THRESHOLD = 100;
            HostID superPeerId = membersOrderByLoadingTime.ElementAt(0).Key;
            foreach (var memberKeyValue in membersOrderByLoadingTime)
            {
                NetClientInfo clientInfo = _netServer.GetClientInfo(memberKeyValue.Key);
                if (clientInfo.recentPingMs <= PING_TIME_MS_THRESHOLD)
                {
                    superPeerId = memberKeyValue.Key;

                    break;
                }
            }

            return superPeerId;
        }

        // RMI ------------------------------------------------------------------------------------------------------------------------
        void InitRMI_BattleServer()
        {
            _C2CStub.Ready = Ready;
            _C2CStub.EndGame = EndGame;
            _C2CStub.Ping = Ping;
            _C2CStub.ForceGoToLobby = ForceGoToLobby;
        }

        bool Ready(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Ready param : " + param);

                do
                {
                    // 어떤 룸인지 검색
                    HostID roomId = (HostID)param["groupId"].ToObject<int>();
                    Room room;
                    _rooms.TryGetValue(roomId, out room);
                    if (room == null || room.isPlaying)
                    {
                        break;
                    }

                    // 해당 룸의 존재하는 유저인지 검색
                    string userId = param["userId"].ToObject<string>();
                    JObject roomUser;
                    room.users.TryGetValue(userId, out roomUser);
                    if (roomUser == null)
                    {
                        break;
                    }

                    // 해당 유저 준비처리 or 준비안됨처리
                    bool ready = param["ready"].ToObject<bool>();
                    roomUser["ready"] = ready;

                    // 그룹원들에게 어떤 유저가 준비를 했는지 통보
                    _C2CProxy.Ready(roomId, RmiContext.ReliableSend, jsonParam);

                    // 클라에서 섞여 보내온 로딩 타임을 remoteUser에 갱신
                    RemoteUser remoteUser;
                    _remoteUsers.TryGetValue(remote, out remoteUser);
                    remoteUser.loadingTime = (float)param["loadingTime"];

                    // 요 밑으로는 배틀 시작 관련 로직 
                    // 2인 이상
                    if (room.users.Count <= 1)
                    {
                        break;
                    }

                    bool checkAllReady = true;
                    foreach (var checkRoomUser in room.users)
                    {
                        JObject checkJRoomUser = JObject.FromObject(checkRoomUser.Value);
                        bool isReady = checkJRoomUser["ready"].ToObject<bool>();
                        if (!isReady)
                        {
                            checkAllReady = false;

                            break;
                        }
                    }

                    // 모든 유저가 준비 되있어야만 함
                    if (!checkAllReady)
                    {
                        break;
                    }
                            
                    
                    Battle battle = new Battle();
                    battle.groupId = room.id;
                    battle.map = room.map;
                    battle.mode = room.mode;
                    battle.vsAI = room.vsAI;

                    // 슈퍼피어 차출하기
                    HostID superPeerId = GetMostSuitableSuperPeerInGroupByLoadingTime(roomId);

                    RemoteUser superPeerRemoteUser;
                    _remoteUsers.TryGetValue(superPeerId, out superPeerRemoteUser);
                    battle.superPeer = superPeerRemoteUser;

                    // 팀 배정을 위한 리스트 셔플 
                    List<KeyValuePair<string, JObject>> roomUsersList = room.users.ToList();
                    List<KeyValuePair<string, JObject>> randomList = new List<KeyValuePair<string, JObject>>();
                    Random r = new Random();
                    int randomIndex = 0;
                    while (roomUsersList.Count > 0)
                    {
                        randomIndex = r.Next(0, roomUsersList.Count);
                        randomList.Add(roomUsersList[randomIndex]);
                        roomUsersList.RemoveAt(randomIndex);
                    }

                    // vs AI 라면 DB에서 AI 데이터를 얻어오기 위한 사전 작업 해주기
                    List<int> aiIndexList = new List<int>();
                    if (battle.vsAI)
                    {
                        var findOnlyServerKnown = CouchbaseManager.Instance.FindConfig("onlyServerKnown");
                        JObject onlyServerKnown = JObject.FromObject(findOnlyServerKnown.Value);
                        int aiCountFromDB = (int)onlyServerKnown["aiCount"];

                        while (aiIndexList.Count < randomList.Count)
                        {
                            int randomAIIndex = r.Next(0, aiCountFromDB);

                            bool aiDuplication = false;
                            foreach (var ai in aiIndexList)
                            {
                                if (ai == randomAIIndex)
                                {
                                    aiDuplication = true;

                                    break;
                                }
                            }
                            if (!aiDuplication)
                            {
                                aiIndexList.Add(randomAIIndex);
                            }
                        }

                        // 유저 한명당 AI 하나이니 2배를 해줘야 한다 
                        battle.personCountForStart = (byte)(room.users.Count * 2);
                    }
                    else
                    {
                        // 시작인원 수로 설정
                        battle.personCountForStart = (byte)room.users.Count;
                    }

                    // 배틀 유저 세팅
                    int teamCount = 0;
                    Team usersTeamIfvsAI = (Team)r.Next(0, 2);  // A,B 둘중 하나를 랜덤으로 골라놓음
                    for (int i = 0; i < randomList.Count; ++i)
                    {
                        var roomUserForBattle = randomList[i];
                        JObject jRoomUserForBattle = JObject.FromObject(roomUserForBattle.Value);
                        int roomUserHostId = (int)jRoomUserForBattle["hostId"];

                        // AI 모드라면 1:1로 매칭된 AI를 만들어서 배틀 유저 목록에 넣어줌
                        if (battle.vsAI)
                        {
                            // AI를 만들어서 할당함 
                            JObject aibattleUser = new JObject();
                            aibattleUser["hostId"] = roomUserHostId;    // 제어권 체크를 위해 이 값을 넣어줘야 함 
                            aibattleUser["ping"] = 0;
                            if (room.mode == GameMode.AMBUSH || room.mode == GameMode.SUPPLY)
                            {
                                // 유저 팀과 반대편으로 배정
                                aibattleUser["team"] = (int)(usersTeamIfvsAI == Team.A ? Team.B : Team.A);
                            }
                            else if (room.mode == GameMode.DEATH_MATCH)
                            {
                                aibattleUser["team"] = (int)Team.INDIVIDUAL;
                            }

                            string aiId = "AI-" + aiIndexList[i];
                            var aiFindResult = CouchbaseManager.Instance.FindUserWithId(aiId);
                            if (aiFindResult.Success)
                            {
                                aibattleUser["user"] = JObject.FromObject(aiFindResult.Value);
                            }
                            battle.users.TryAdd(aiId, aibattleUser);
                        }


                        JObject battleUser = new JObject();
                        battleUser["hostId"] = roomUserHostId;
                        battleUser["ping"] = _netServer.GetLastUnreliablePingMs((HostID)roomUserHostId);
                        battleUser["user"] = jRoomUserForBattle["user"];

                        // 팀 배정
                        if (battle.vsAI)
                        {
                            if (room.mode == GameMode.AMBUSH || room.mode == GameMode.SUPPLY)
                            {
                                // AI 모드라면 유저들은 전부 같은 팀을 맺음
                                battleUser["team"] = (int)usersTeamIfvsAI;
                            }
                            else if (room.mode == GameMode.DEATH_MATCH)
                            {
                                battleUser["team"] = (int)Team.INDIVIDUAL;
                            }
                            // AI 컨트롤 제어권 넘김
                            string aiId = "AI-" + aiIndexList[i];
                            battleUser["controlAIId"] = aiId;
                        }
                        else
                        {
                            if (room.mode == GameMode.AMBUSH || room.mode == GameMode.SUPPLY)
                            {
                                // 처음부터 반까지는 A, 반에서부터 끝까지는 B로 배정
                                battleUser["team"] = (teamCount < randomList.Count / 2) ? (int)Team.A : (int)Team.B;
                            }
                            else if (room.mode == GameMode.DEATH_MATCH)
                            {
                                battleUser["team"] = (int)Team.INDIVIDUAL;
                            }
                            teamCount++;
                        }

                        battle.users.TryAdd((string)jRoomUserForBattle["user"]["id"], battleUser);
                    }

                    _battles.TryAdd(room.id, battle);

                    // 배틀 시간 기록을 위한 스탑워치 시작
                    battle.stopwatch.Start();

                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);

                    // 수퍼피어 퍼즈 판정을 위한 타이머 시작
                    // 타이머는 생성을 하면 처음에 바로 한번 실행하고 이후 주기적으로 호출한다
                    // 게임을 바로 시작하지 않고 5초 카운트다운을 하고 시작하기 때문에 5초 이후 + checkSuperPeerChangeTimeMs 로 첫 호출이 되어야만 한다
                    // 이미 게임을 시작할 때 수퍼피어를 정해주고 내려주기 떄문에 바로 수퍼피어를 체크하지 않아도 됨
                    // 또한 수퍼피어 핑 지연 체크용 스탑워치를 여기서 시작하기 때문에 수퍼피어 체크가 호출되면 100%로 수퍼피어가 변경되버린다 
                    int checkSuperPeerChangeTimeMs = (int)config["checkSuperPeerChangeTimeMs"];
                    int CHECK_PING_TIMER_WAIT_TIME_MS = 5000 + checkSuperPeerChangeTimeMs;
                    int CHECK_PING_TIMER_PERIOD_TIME_MS = checkSuperPeerChangeTimeMs;
                    System.Threading.TimerCallback timerDelegate = new System.Threading.TimerCallback(CheckSuperPeer);
                    battle.superPeerCheckTimerReference = new System.Threading.Timer(timerDelegate, battle, CHECK_PING_TIMER_WAIT_TIME_MS, CHECK_PING_TIMER_PERIOD_TIME_MS);
                    battle.stopwatchForSuperPeerCheck.Start();

                    // 해당 룸 플레이방으로 전환
                    room.isPlaying = true;

                    // 방 인원 전부에게 배틀이 시작됬음을 통지
                    string jsonResult = JsonConvert.SerializeObject(battle);
                    Console.WriteLine("StartGame : " + jsonResult);
                    _C2CProxy.StartGame(roomId, RmiContext.ReliableSend, jsonResult);
                        
                    

                } while (false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool EndGame(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("EndGame param : " + param);

                JObject result = new JObject();

                do
                {

                    // 시작했던 배틀이 맞는지 찾기
                    HostID battleId = param["groupId"].ToObject<HostID>();
                    Battle battle;
                    _battles.TryGetValue(battleId, out battle);
                    if (battle == null)
                    {
                        break;
                    }

                    // 룸 찾기
                    Room room;
                    _rooms.TryGetValue(battleId, out room);
                    if (room == null)
                    {
                        break;
                    }

                    // 수퍼피어가 동일한 녀석인지 체크
                    if (battle.superPeer.hostId != remote)
                    {
                        break;
                    }

                    var statistics = param["statistics"].ToObject<Dictionary<string, JObject>>();

                    // config 
                    var findConfig = CouchbaseManager.Instance.FindConfig("static");
                    JObject config = JObject.FromObject(findConfig.Value);

                    // 골드 보상, 경험치 책정 준비 
                    int killReward = (int)config["killReward"];
                    int deathReward = (int)config["deathReward"];
                    int victoryReward = (int)config["victoryReward"];
                    int battleParticipationReward = (int)config["battleParticipationReward"];

                    int killExp = (int)config["killExp"];
                    int deathExp = (int)config["deathExp"];
                    int victoryExp = (int)config["battleVictoryExp"];
                    int battleParticipationExp = (int)config["battleParticipationExp"];

                    // NotifyEndGame때 얼마의 보상을 땄는지 알려주기 위해 이 작업이 필요함
                    result["earn"] = new JObject();
                    result["earnExp"] = new JObject();
                    foreach (var user in statistics)
                    {
                        // 킬, 데스, 배틀 참여 보상은 미리 세팅해놓음
                        byte kill = (byte)user.Value["kill"];
                        byte death = (byte)user.Value["death"];

                        result["earn"][user.Key] = kill * killReward + death * deathReward + battleParticipationReward;
                        result["earnExp"][user.Key] = kill * killExp + death * deathExp + battleParticipationExp;
                    }

                    if (battle.mode == GameMode.AMBUSH)
                    {
                        byte VICTORY_ROUND = config["ambush_victoryRound"].ToObject<byte>();
                        int maxKillAndDeath = battle.personCountForStart / 2 * VICTORY_ROUND;     // A/B 두팀이니깐 /2를 해주는거임 

                        byte teamAWinRound = param["teamAWinRound"].ToObject<byte>();
                        byte teamBWinRound = param["teamBWinRound"].ToObject<byte>();

                        foreach (var user in statistics)
                        {
                            byte kill = (byte)user.Value["kill"];
                            byte death = (byte)user.Value["death"];

                            // 킬데스가 체크되고, 최신 유저DB 읽기에 성공했다면 DB에 기록
                            bool validate = kill <= maxKillAndDeath && death <= maxKillAndDeath;
                            if (validate)
                            {
                                JObject battleUser;
                                battle.users.TryGetValue(user.Key, out battleUser);
                                if (battleUser == null)
                                {
                                    continue;
                                }

                                bool win, lose, draw; win = lose = draw = false;

                                Team team = battleUser["team"].ToObject<Team>();
                                if (teamAWinRound == teamBWinRound)
                                {
                                    // 무승부
                                    draw = true;
                                }
                                else if ((teamAWinRound > teamBWinRound && team == Team.A) || (teamBWinRound > teamAWinRound && team == Team.B))
                                {
                                    // 승리
                                    win = true;

                                    // 승리 추가 보상
                                    result["earn"][user.Key] = (int)result["earn"][user.Key] + victoryReward;
                                    result["earnExp"][user.Key] = (int)result["earnExp"][user.Key] + victoryExp;
                                }
                                else
                                {
                                    // 패배
                                    lose = true;
                                }

                                // 유저 업데이트 
                                UpdateUserAfterBattle(room, user.Key, win, lose, draw, kill, death, (int)result["earn"][user.Key], (int)result["earnExp"][user.Key]);
                            }
                        }
                    }
                    else if (battle.mode == GameMode.SUPPLY)
                    {
                        int TEAM_LIFE = (int)config["supply_teamLife"] * (battle.personCountForStart / 2);

                        byte teamARemainLife = param["teamARemainLife"].ToObject<byte>();
                        byte teamBRemainLife = param["teamBRemainLife"].ToObject<byte>();

                        foreach (var user in statistics)
                        {
                            byte kill = (byte)user.Value["kill"];
                            byte death = (byte)user.Value["death"];

                            // 킬데스가 체크되고, 최신 유저DB 읽기에 성공했다면 DB에 기록
                            bool validate = kill <= TEAM_LIFE && death <= TEAM_LIFE;
                            if (validate)
                            {
                                JObject battleUser;
                                battle.users.TryGetValue(user.Key, out battleUser);
                                if (battleUser == null)
                                {
                                    continue;
                                }

                                bool win, lose, draw; win = lose = draw = false;

                                Team team = battleUser["team"].ToObject<Team>();
                                if (teamARemainLife == teamBRemainLife)
                                {
                                    // 무승부
                                    draw = true;
                                }
                                else if ((teamARemainLife > teamBRemainLife && team == Team.A) || (teamBRemainLife > teamARemainLife && team == Team.B))
                                {
                                    // 승리
                                    win = true;

                                    // 승리 추가 보상
                                    result["earn"][user.Key] = (int)result["earn"][user.Key] + victoryReward;
                                    result["earnExp"][user.Key] = (int)result["earnExp"][user.Key] + victoryExp;
                                }
                                else
                                {
                                    // 패배
                                    lose = true;
                                }

                                // 유저 업데이트 
                                UpdateUserAfterBattle(room, user.Key, win, lose, draw, kill, death, (int)result["earn"][user.Key], (int)result["earnExp"][user.Key]);
                            }
                        }
                    }
                    else if (battle.mode == GameMode.DEATH_MATCH)
                    {
                        // 가장 높은 킬을 기록한 사람만 승리 반영 (공동 1위인지 체크 필요)
                        int OBJECTIVE_KILL = (int)config["deathMatch_objectiveKill"];

                        int bestKill = 0;
                        Dictionary<string, string> bestKillersId = new Dictionary<string, string>();
                        foreach (var user in statistics)
                        {
                            int userKill = (int)user.Value["kill"];
                            if (userKill > bestKill)
                            {
                                bestKill = userKill;

                                bestKillersId.Clear();
                                bestKillersId.Add(user.Key, user.Key);
                            }
                            else if (userKill == bestKill)
                            {
                                bestKillersId.Add(user.Key, user.Key);
                            }
                        }

                        foreach (var user in statistics)
                        {
                            byte kill = (byte)user.Value["kill"];
                            byte death = (byte)user.Value["death"];

                            // 킬데스가 체크되고, 최신 유저DB 읽기에 성공했다면 DB에 기록
                            bool validate = kill <= OBJECTIVE_KILL;
                            if (validate)
                            {
                                bool win, lose, draw; win = lose = draw = false;

                                if (bestKillersId.ContainsKey(user.Key) && bestKillersId.Count == 1)
                                {
                                    // 승리
                                    win = true;

                                    // 승리 추가 보상
                                    result["earn"][user.Key] = (int)result["earn"][user.Key] + victoryReward;
                                    result["earnExp"][user.Key] = (int)result["earnExp"][user.Key] + victoryExp;
                                }
                                else if (bestKillersId.ContainsKey(user.Key) && bestKillersId.Count > 1)
                                {
                                    // 무승부
                                    draw = true;
                                }
                                else
                                {
                                    // 패배
                                    lose = true;
                                }

                                // 유저 업데이트 
                                UpdateUserAfterBattle(room, user.Key, win, lose, draw, kill, death, (int)result["earn"][user.Key], (int)result["earnExp"][user.Key]);
                            }
                        }
                    }

                    // 배틀에서 삭제
                    Battle removeBattle;
                    _battles.TryRemove(battleId, out removeBattle);
                    removeBattle.Dispose();

                    /*
                    // 결과 로그 기록
                    // [시간, 총 배틀 시간, result]
                    removeBattle.stopwatch.Stop();
                    uint battleTimeSec = (uint)removeBattle.stopwatch.Elapsed.TotalSeconds;

                    JObject battleLogDB;
                    JArray battlesDB;

                    DateTime localDate = DateTime.Now;
                    var findBattleLog = CouchbaseManager.Instance.FindBattleLogWithDate(localDate.ToShortDateString());
                    if (findBattleLog.Success)
                    {
                        battleLogDB = JObject.FromObject(findBattleLog.Value);
                        battlesDB = JArray.FromObject(battleLogDB["battles"]);
                    }
                    else
                    {
                        // 없으면 새로 생성하기
                        battleLogDB = new JObject();
                        battleLogDB["id"] = localDate.ToShortDateString();

                        battlesDB = new JArray();
                    }

                    JObject newBattleLog = new JObject();
                    newBattleLog["time"] = localDate.ToShortTimeString();
                    newBattleLog["map"] = removeBattle.map.ToString();
                    newBattleLog["mode"] = removeBattle.mode.ToString();
                    newBattleLog["battleTimeSec"] = battleTimeSec;
                    newBattleLog["result"] = param;
                    battlesDB.Add(newBattleLog);
                    battleLogDB["battles"] = battlesDB;

                    CouchbaseManager.Instance.UpsertBattleLog(battleLogDB);
                    */

                    // 대기방으로 전환
                    room.isPlaying = false;

                    // 룸으로 돌아가기 위해서 모든 유저들 레디 해제
                    foreach (var roomUser in room.users)
                    {
                        roomUser.Value["ready"] = false;
                    }

                    // 룸을 통지 
                    result["groupId"] = (int)battleId;
                    result["room"] = JObject.FromObject(room);
                    string jsonResult = JsonConvert.SerializeObject(result);
                    _C2CProxy.EndGame(battleId, RmiContext.ReliableSend, jsonResult);

                } while (false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool Ping(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 
                JObject param = JObject.Parse(jsonParam);
                Console.WriteLine("Ping param : " + param);

                // 시작했던 배틀이 맞는지 찾기
                HostID battleId = param["groupId"].ToObject<HostID>();
                Battle battle;
                _battles.TryGetValue(battleId, out battle);

                if (battle != null)
                {
                    // 수퍼피어가 동일한 녀석인지 체크
                    if (battle.superPeer.hostId == remote)
                    {
                        // 스탑워치 재시작 
                        battle.stopwatchForSuperPeerCheck.Restart();
                    }
                    else
                    {
                        // 수퍼피어 아니다
                    }
                }
                else
                {
                    // 배틀이 없다
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }

        bool ForceGoToLobby(HostID remote, RmiContext rmiContext, string jsonParam)
        {
            System.Threading.Monitor.Enter(this);

            try
            { 

                // 룸 나가는게 중복일 것 같지만... 로직상 둘 중 하나만 처리하게 되어있다 
                // 귀찮아서 그냥 이렇게 뒀다 
                ForceLeaveRoom(remote);
                ForceLeaveBattleAndRoom(remote);

            
                // 음... 배틀과 룸에서 나와야해서 C2C에 두긴 했지만 사실 C2C 정보가 아무 필요 없다
                RemoteUser leaveRemoteUser;
                _remoteUsers.TryGetValue(remote, out leaveRemoteUser);
                if (leaveRemoteUser != null)
                {
                    // 갱신된 유저 정보 내려주기 
                    var findResult = CouchbaseManager.Instance.FindUserWithId(leaveRemoteUser.userId);
                    if (findResult.Success)
                    {
                        JObject user = JObject.FromObject(findResult.Value);

                        JObject result = new JObject();
                        result["user"] = user;

                        string jsonResult = JsonConvert.SerializeObject(result);
                        Console.WriteLine("ForceGoToLobby result : " + jsonResult);

                        _C2CProxy.ForceGoToLobby(remote, RmiContext.ReliableSend, jsonResult);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            System.Threading.Monitor.Exit(this);

            return true;
        }
    }
}



/*
     수퍼피어를 찾자 해킹에 위험이 있다 하더라도 충돌처리나 아이템 스폰같은건 수퍼피어가 해야해
     클라이언트상에서만 판단할 수 밖에 없는 것들만 판단하고, 서버에서 할 수 있는 부분은 서버가 한다 
     클라이언트 - spawnmanager(수퍼피어와 연계), scenereset, 통계, 캐릭터(이름, 핑), 스코어, 채팅, 조작, 체력, 총알, 설정, 재장전
     피어 - completereadyingame, move, shot, reload
     수퍼피어 - damage, kill, death, spawnitem, eatitem, endround
     서버 - startgame, endgame, 통계(kill,death,점수,라운드), 수퍼피어의 모든 요청 판정(현재 수퍼피어가 보낸건지 확인하고 그룹에게 송신)
            수퍼피어검색(연결 끊어졌을 시 다른 피어에게 인계, 라운드별로 체크해서 인계, 주기적으로 체크해서 인계)
            startround(completeload, endround를 가지고 판정 kill/death가 정확히 맞아야 함)
            spawnuser 기습모드: completereadyingame를 요청한 유저들에 대해서 송신. 전부 다 됬으면 startround
                      집단전: 유저가 death되면 타이머를 통해 spawnuser 송신

     수퍼피어 선정 
     애들이 방에서 준비 누를 때마다 얻어오고, startgame전에 한번 더 체크해서 확실하게 선정
     게임 시작하고나서도 통신 상태는 언제든 바뀔 수 있기 때문에 수시로 체크하는게 필요할 것 같은데
     바로 인계를 할 수 있는 시스템이 마련되긴 해야 함 중간에 슈퍼피어가 나가버릴 수 있기 때문임
     selectsuperpeer 그렇기 때문에 서버에서 이를 판단해서 특정 피어에게 피어를 선정하는 함수가 필요함
     이걸 받은 피어는 판정 처리를 보내는 권한을 갖게 됨
     deselectsuperpeer 이전 수퍼피어에게 권한을 해제함. 접속이 끊겨서 탈락됬을 때는 안보내도 됨
     spawnmanager 얘를 어떻게 시작할지 말지 정하지.. 수퍼피어는 언제라도 바뀔 수 있기 때문임
     현재 플레이중인지 대기중인지에 대한 상태 bool 변수를 누군가 가지고 있어야 함 
     라운드 현황같은 통계 현황을 관리하는 클래스가 필요하긴 하다
     lobby, entry처럼 battle 하나 들고있자 너가 플레이어별 점수, 스코어, 라운드 관리를 해라 모드에 따라서도 달라지는데
     차라리 모드별 매니저를 둬버릴까 맵은 재활용할꺼고 그래.. 모드 매니저를 두는게 좋겠어
     근데 달라지는게 뭐가 있지 킬뎃, 개인당 점수, 라운드
     모드매니저는 굳이 필요없겠구먼 아직까지는
     결론은 battle 클래스에서 isplaying을 관리하고 spawnmanager를 킬 때는 이 값을 참조해서 start하면 된다     
     우려되는 점: 애들 통신이 전부 좋다거나 전부 안좋아서 너무 자주 수퍼피어가 바뀌는 경우 
                  (몇분의 한번꼴로 체크되서 바뀌게 하자. 다른 원인으로 수퍼피어가 중간에 바뀐 경우 타이머를 다시 리셋)
                  수퍼피어가 전환되는 그 시간에 어떤 판정이 생길 경우, 그 판정이 무시될 수 있는 문제

     중간에 유저가 leave된 경우 
     얘를 들고 있는 방을 찾아야 함 
     키가 hostid가 아닌 userid로 되어있기 때문에 일일히 방 전부 foreach돌리고 users도 foreach돌리고 hostid 대조해봐야 함...
     방에 들어갈 때 hostid를 키고 가지고, roomid, userid를 값으로 가지는 딕셔너리가 필요함
     이 딕셔너리에 없는거면 방에서 나온게 아니라 그냥 로비나 엔트리에서나 머물다 나간거니 아무처리 안해줘도 됨
     유저를 찾았다면 방이 플레이방인지 대기방인지 확인하고 
     대기방이라면 방에 나가기 처리를 해주면 됨
     플레이 방이라면 수퍼피어였는지를 확인. 수퍼피어였다면 다른 애로 빨리 바꿔줘야 함 
     이후 db에다가 패배처리를 기록해서 다음 접속 시 패널티 물도록 함
     그리고 다른 p2p 구성원들에게 leave를 날림

     점수 정산
     수퍼피어가 보내온 판정 요청들을 보고, db에서 읽어온 점수 판정 기준을 조회하고 처리함
     판정 점수를 기록해서 각각 체크를 해서 피어에게 보내주면 알아서 해독한 후 점수 정산 텍스트를 표시 
     스태틱 데이터를 mysql로 할지, 카우치베이스로만 할지 고민이다 카우치베이스로도 충분하긴한데 수정도 쉽고 

     어뷰징
     반드시 endgame까지 도달해야만 백신이 주어짐. 최소 하나를 줘야하는 이유는 족같아서 나가는 사람이 생기기 때문임
     게임 도중 나간 경우 어떻게 해줄까... 패널티를 주긴 해야하는데 강제로 패배처리를 시켜버릴까
     아니면 패널티를 주진 않지만 남아있는 사람에게 혜택을 주는게 좋을까
     둘다 하는게 맞는 것 같은데 악의적으로 자꾸 나가면 패널티를 줘야하고 
     대신 남아있는 사람에게 혜택을 주되 그 혜택의 보상은 백신 제한을 걸어둬서 어뷰징을 막는 개념임 

     spawnmanager 
     타이머 코루틴 돌아가며 체력/탄약을 스폰함 
     스폰 리스트는 차일드로 가지고 있음 랜덤으로 그 스폰리스트 중 하나를 선택해서 스폰함
     같은 곳에 여러번 스폰이 될 수 있긴 하지만 그냥 그려러니 하자 
     플레이중인지 아닌지에 따라 스폰될지 안될지를 정해야하지만... 근데 사실 라운드마다 리셋될꺼라 라운드 종료됬다해서 끌 필요는 없을 듯
     start, stop만 있으면 될 듯 생성된 아이템 제거는... items라는 빈 게임오브젝트를 만들고 얘 밑으로 생성되게 한 후 얘를 이용해 리셋시키자
     단지 아이템 스폰 위치만 관리하고 아이템을 인스턴스만 시키는 역할이기 때문에 
     아이템이 먹어지는건 아이템에게 있어서 충돌 태그가 유저라면 바로 사라지고
     유저에게 있어 충돌이 아이템이라면 아이템 먹었다는 판정을 보내고(수퍼피어만) 서버에서 확인이 들어오면 먹는 판정이 되는 형태로 간다 
     아이템 뿐만 아니라 모든 판정이 "처리 요청 -> 서버에서 처리 후 전달" 개념
     얘가 유저 스폰까지 책임지도록 해야할 듯 유저 스폰위치도 들고 있게 하자

     진행 
     startgame 서버에서 애들 준비가 완료되면 전달. 피어들이 받으면 씬 로딩, 유저 캐릭터 정보 받아서 세팅. 룸 isplaying=true 
     completereadyingame 위에 피어들 세팅이 전부 완료되면 송신. 지들끼리 수신받으면서 ui 표시? 아님 그냥 넘어가도 됨
     startround 서버에서 유저 준비가 완료되면 라운드 시작 사인 전송. 피어들은 게임 시작하고 수퍼피어는 스폰매니저 발동하면서 본격적인 게임 플레이
     플레이하면서 수퍼피어가 보내 온 처리를 서버는 현재 수퍼피어가 보내온게 맞는지 확인하고 그룹 전부에게 송신. 통계도 잡아야 함
     endround 서버에서는 라운드 종료가 맞는지 kill, death 체크를 하고 송신 (중간에 유저가 나가는 부분 유의)
              진짜로 라운드 종료가 맞다면 서버에서 모두에게 송신. 라운드가 모두 끝났다면 endround가 아닌 endgame을 보낸다
              endgame 때는 서버에서 체크한 통계를 보내준다 정산된 백신 갯수까지. 서버에서는 결과를 db에 기록해야함
              룸 isplaying=false, 룸 유저들 ready=false
     피어들은 endround가 들어오면 5초 코루틴 돌림, 시간되면 맵 리셋 후 completereadyingame를 보내줌. 똑같은 형식으로 게임이 종료될 때까지 반복 
     endgame이 10초 코루틴 돌림 그때까지 결과창 띄우고, 완료되면 룸으로 복귀(복귀될 때 채팅으로 복귀됬다고 알려줄까)

    Kill, Death... 둘다 어쩌면 같은 의미인데 사람이 사람한테 죽는거니까 
    둘을 따로따로 두면 Kill/Death 통계처리가 정확하지도 않고 트래픽만 두번 오게 됨
    groupId, doerId, sufferId
    고작 몇개 안되긴하지만 바로 유저에 접근할 수 있는게 좋은 것 같다
    팀단위롬 딕셔너리를 구성하지 말고 그냥 users 통짜 하나에다가 개인이 Team을 가지고 있는게 나아보임 그래야 접근이 편해짐
    클라에서는 통계치 확인해줘야할때만 값 얻어와서 보여주기만 하면 되니께

    Damage
    groupId, doerId, sufferId  도움 점수 줄 때 기록해둘 필요있음
    데미지 몇짜리인지 알려줘야겠군
    그리고 Damage, Kill 두개 겹쳐서 보내면 안된다
    수퍼피어에서 데미지 계산을 할 때 이거 맞고 뒤진다면 Kill, 이거 맞고 산다면 Damage인거다

    정보는 전부 StartGame에서 주고
    SpawnUser때는 그냥 userId만 줘서 스폰시키는 역할만하도록 하는게 나은 것 같기도 함

    흠... 근데 생각보다 트래픽 부하가 너무 심할 것 같네 
    판정처리는 그냥 수퍼피어가 알아서 하고 수퍼피어 체인지만 서버에서 해주는게 좋을 것 같다
    일단 서버에 P2P그룹을 빼버리지만, Stub은 받을 수 있게 해놓자 보내는 것만 HostID_Server로 보내면 가긴갈꺼야
    다만, 그룹으로 보내기 했을 때 못 받는거지
    수퍼피어만큼은 서버에서 관리되게 해버릴까 서버의 트래픽 엄청 몰리는 것보단 나은 최선의 선택인 것 같음
    모든 애들한테 수퍼피어가 누구인지를 알고 있게 해야하고 P2P멤버나갔을 때 이를 감지해서 잠시 일시정지하고 서버에서 수퍼피어를 선정해주길 기다려야 함
    수퍼피어에 대한것만 해주고 나머지는 전부 니들 알아서 해라 이런식이 되야하는건가
    최종적으로 게임이 끝났을 때만 수퍼피어가 서버로 보내서 결과를 통보하고 그 결과에 대해서만 리미트를 판단을 하고 내려주도록 하자 
    로그로 기록해서 나중에 해킹 유저 판단에도 도움이 되게 하자
    이것도 좀 우려되는게 있다면 악의적으로 폰 몇개로 비공개방 만들어서 한방에 들어가서 어뷰징하거나 해킹할 수 있음
    수퍼피어 특성상 막는데 한계가 있는 부분이긴 함
    로그에 기록된 시간과 시간의 차이를 통해서도 해킹 여부를 탐지해야겠구먼

    클라이언트에서 어떤 피어가 나갔으면 HostId가 넘어올 것이다
    users를 전부 뒤져서 hostId가 같은 녀석 검색
    그 녀석이 수퍼피어라면 모든 동작 그만. 날려버리기도 함 
    일반 피어라면 그냥 날려버리고 똑같이 진행

    클라이언트에서 P2P를 찾아낼 수 있다면 굳이 서버에서 P2P그룹원들에게 쏴줄 필요가 없겠군

    로그 쓸 때는 유저 정보 다 보내는게 아니라 userId만 기록 해야한다 
    결과가 들어오면 DB에서 정보 얻어와서 제대로 맞는지만 확인하면 되는데 그리고 로그 쌓고
*/
